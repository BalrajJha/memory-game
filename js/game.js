window.addEventListener("load", () => {
    const startButton = document.querySelector(".start-button");
    const numberOfCardsInput = document.getElementById("modifier");
    
    refreshHighScoreHeader();

    startButton.addEventListener("click", (e) => {
        const value = numberOfCardsInput.querySelector("input");

        // value should only contain number between  2 and 60 and should be even
        // should be a number
        if (value.value.length === 0 || value.value.indexOf(" ") > -1 || /^\d+$/.test(value.value) === false
            || Number(value.value) < 2 || Number(value.value) > 60 || Number(value.value) % 2 !== 0) {
                // regex checks for a positive whole number and rejects strings
                // https://stackoverflow.com/questions/175739/how-can-i-check-if-a-string-is-a-valid-number
                console.log("Invalid input");
        } else {
            // success
            const title = document.getElementById("title");
            title.style.display = "none";
            startButton.style.display = "none";
            numberOfCardsInput.style.display = "none";
            start();
        }
        
    });

});



function start() {
    let gameBoard = document.querySelector(".game-board");
    let gameData = document.querySelector(".game-data"); 
    const goldenAngle = 180 * (3 - Math.sqrt(5));
    let nColors = Number(document.getElementById("modifier").querySelector("input").value);


    function generateUnshuffledColors(nColors) {
        nColors = nColors / 2;
        let unshuffledColors = new Array(nColors).fill(0);

        unshuffledColors.map((element, index) => {
            // https://stackoverflow.com/questions/10014271/generate-random-color-distinguishable-to-humans
            // unshuffledColors[index] = `hsl(${index * goldenAngle + 60}, 100%, 75%)`;
            // const randomNumber = Math.floor(Math.random() * 999);
            unshuffledColors[index] = `hsl(${index * goldenAngle + 60}, 100%, 75%)`;

            return "";
        });

        unshuffledColors = unshuffledColors.concat(unshuffledColors);

        return unshuffledColors;
    }
    
    let unshuffledColors = generateUnshuffledColors(nColors);

    const restartButton = document.getElementById("restart-button");
    const backgroundGradient = "radial-gradient(circle, rgba(87,10,87,1) 0%, rgba(169,16,121,1) 35%, rgba(248,6,204,1) 100%)";

    // game state
    let currentCount = 0;
    let correctGuess = 0;
    let previousClick = null;

    gameBoard.style.display = "flex";
    gameData.style.display = "block";

    // shuffle the array
    function shuffleArray(unshuffledColors) {
        const usedIndexes = new Set();
        const colors = new Array(unshuffledColors.length);
        let iterateIndex = 0;

        while(usedIndexes.size !== colors.length) {
            // generate random number between 0 and the length of the array
            const randomIndex = Math.floor(Math.random() * unshuffledColors.length);

            if (usedIndexes.has(randomIndex)) {
                continue;
            } else {
                colors[iterateIndex] = unshuffledColors[randomIndex];
                iterateIndex++;
                usedIndexes.add(randomIndex); 
            }
        }
        console.table(unshuffledColors);
        console.table(colors);
        return colors;
    }
    
    let colors = shuffleArray(unshuffledColors);

    // create a card and add it to game board
    function createCard(color) {
        const div = document.createElement("div");

        div.dataset.trueColor = color;
        // div.style.width = String(Math.floor(100 / Math.floor(colors.length / 2))) + "%";
        div.classList.add("card");
        gameBoard.appendChild(div);
    }

    function addColorToCard(colors) {
        colors.map((color) => {
            createCard(color);

            return;
        });
    }

    function displayCurrentScore(currentCount) {
        const currentScore = document.querySelector(".current-score");
        currentScore.textContent = "Score " + currentCount;
    }

    function setHighScore(currentScore) {
        if (sessionStorage.getItem("highScore") === null) {
            sessionStorage.setItem("highScore", String(currentScore));

            const currentScoreElement = document.querySelector(".current-score");
            currentScoreElement.textContent = `Score ${currentScore}` + "\r\nNew High Score🎉";

            console.log("OH NO");
        } else {
            if (currentScore < sessionStorage.getItem("highScore")) {
                sessionStorage.setItem("highScore", String(currentScore));
                const currentScoreElement = document.querySelector(".current-score");
                currentScoreElement.textContent = `Score ${currentScore}` + "\r\nNew High Score🎉";
            }   
        }
    }

    function resetGameState() {
        currentCount = 0;
        correctGuess = 0;
        previousClick = null;
    }

    function restartGame() {
        displayCurrentScore(currentCount);
        resetBoard();
        unshuffledColors = generateUnshuffledColors(nColors);
        console.log(unshuffledColors);
        colors = shuffleArray(unshuffledColors);
        refreshHighScoreHeader();
        addColorToCard(colors);
        gameLoop();
    }

    function resetBoard() {
        gameBoard.remove();

        // create div for game board
        gameBoard = document.createElement("div");
        gameBoard.classList.add("game-board");
        document.body.appendChild(gameBoard);
    }

    function gameLoop() {
        // hide restartButton and show gameBoard
        restartButton.style.display = "none";
        gameBoard.style.display = "flex";

        // delegate event to gameBoard
        gameBoard.addEventListener(("click"), (e) => {
            if (e.target.classList.contains("card")) {
                const card = e.target;

                // user doesn't click on the same card
                if (previousClick !== card) {
                    currentCount++;

                    displayCurrentScore(currentCount);
                    console.log(currentCount);
                    card.style.background = card.dataset.trueColor;
                    card.dataset.trueColor = backgroundGradient;
                    if (currentCount % 2 === 0) {
                        // disable mouse clicks on gameboard if two cards are selected
                        gameBoard.style.pointerEvents = 'none';

                        if (previousClick.style.background === card.style.background) {
                            // colors are matched then keep showing card's true color
                            // enable mouse clicks again
                            gameBoard.style.pointerEvents = 'auto';

                            // uncomment this if you want 1 sec delay even after a correct match
                            // removing this as 1 sec delay after a match doesn't feel right
                            // setTimeout(() => {
                            //     gameBoard.style.pointerEvents = 'auto';
                            // }, 1000);

                            correctGuess += 2;

                            // disable click on the matched card itself
                            // this solves a bug where if you selected an unmatched card and then a matched card
                            // then the matched card will become facedown
                            previousClick.style.pointerEvents = 'none';
                            card.style.pointerEvents = 'none';

                            console.log("You made a match!");

                            // GAME WON
                            if (correctGuess === colors.length) {
                                setHighScore(currentCount);
                                // document.body.innerText = `Game Won: Score: ${currentCount}`;
                                resetGameState();

                                restartButton.style.display = "block";
                                gameBoard.style.display = "none";
                                restartButton.addEventListener('click', restartGame);
                                
                            }
                        } else {
                            // colors not matched then reset cards
                            setTimeout(() => {
                                previousClick.dataset.trueColor = previousClick.style.background;
                                previousClick.style.background = backgroundGradient;
                                card.dataset.trueColor = card.style.background;
                                card.style.background = backgroundGradient;
                                //enable mouse clicks again
                                gameBoard.style.pointerEvents = 'auto';
                                previousClick = null;
                            }, 1000);
                        }
                        
                    } else if (currentCount % 2 !== 0) {
                        // only set previous if number of cards selected is 1
                        previousClick = e.target;
                    }
                }else {
                    console.log("Press a diff button");
                }
            } 
        });
    }



    addColorToCard(colors);
    gameLoop();
}

// this will update the text of the header displaying high score
function refreshHighScoreHeader() {
    const highScoreElement = document.getElementById("high-score");
    console.log(highScoreElement);
    if (sessionStorage.getItem("highScore") === null) {
        highScoreElement.innerText = "High Score -";
        console.log("OH NO");
    } else {
        highScoreElement.innerText = "High Score " + sessionStorage.getItem("highScore");
    }
}

